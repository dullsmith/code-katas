package leetcode;

//Source: https://leetcode.com/problems/valid-anagram/


import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ValidAnagram {

    public boolean isAnagram(String source, String target) {
        //Least resource consumption
        if (source.length() != target.length()) {
            return false;
        }

        char[] sortedSource = source.toCharArray();
        Arrays.sort(sortedSource);
        char[] sortedTarget = target.toCharArray();
        Arrays.sort(sortedTarget);

        return Arrays.equals(sortedTarget, sortedSource);
    }

    public boolean isAnagramNotMine(String source, String target) {
        if (source.length() != target.length()) {
            return false;
        }
        var sourceMap = source.chars()
                .boxed()
                .collect(Collectors.groupingBy(it -> it))
                .entrySet()
                .stream()
                .collect(Collectors.toMap(it -> Character.toString(it.getKey()), it -> it.getValue().size()));

        var targetMap = target.chars()
                .boxed()
                .collect(Collectors.groupingBy(it -> it))
                .entrySet()
                .stream()
                .collect(Collectors.toMap(it -> Character.toString(it.getKey()), it -> it.getValue().size()));

        return sourceMap.equals(targetMap);
    }

    public boolean isAnagramFunctional(String source, String target) {
        if (source.length() != target.length()) {
            return false;
        }

        var sourceMap = source.chars()
                .mapToObj(it -> (char) it)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        var targetMap = target.chars()
                .mapToObj(it -> (char) it)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        return sourceMap.equals(targetMap);
    }


}
