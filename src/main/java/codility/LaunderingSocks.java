package codility;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class LaunderingSocks {

    public int solution(int capacity, int[] clean, int[] dirty) {

        System.out.println("W: " + capacity + "; C: " + Arrays.toString(clean) + "; D: " + Arrays.toString(dirty));

        int cleanMatches = 0;
        Map<Integer, Integer> cleanSocksByColor = new ConcurrentHashMap<>();
        Map<Integer, Integer> dirtySocksByColor = new ConcurrentHashMap<>();

        // pile all socks by color
        for (int i : clean) {
            cleanSocksByColor.put(i, cleanSocksByColor.getOrDefault(i, 0) + 1);
        }
        for (int i : dirty) {
            dirtySocksByColor.put(i, dirtySocksByColor.getOrDefault(i, 0) + 1);
        }

        System.out.println("CleanPile: " + cleanSocksByColor);
        System.out.println("DirtyPile: " + dirtySocksByColor);


        cleanMatches += cleanSocksByColor.values()
                .stream()
                .map(it -> it / 2)
                .reduce(0, Integer::sum);

        cleanSocksByColor = cleanSocksByColor.entrySet().stream()
                .filter(it -> (it.getValue() % 2) != 0)
                .collect(Collectors.toMap(Map.Entry::getKey, it -> 1));


        System.out.println("CleanMatches: " + cleanMatches);
        System.out.println("CleanPile: " + cleanSocksByColor);

        for (int sockColor : cleanSocksByColor.keySet()) {
            if (capacity > 0) {
                if (dirtySocksByColor.containsKey(sockColor)) {
                    capacity--;
                    cleanMatches++;
                    if (dirtySocksByColor.get(sockColor) == 1) {
                        dirtySocksByColor.remove(sockColor);
                    } else {
                        dirtySocksByColor.put(sockColor, dirtySocksByColor.get(sockColor) - 1);
                    }
                }
            }
        }

        System.out.println("CleanMatches: " + cleanMatches);
        System.out.println("DirtyPile: " + dirtySocksByColor);

        if (capacity > 1) {
            capacity = (capacity / 2);
            int dirtyPairs = 0;
            for (int sockColor : dirtySocksByColor.keySet()) {
                if (dirtySocksByColor.get(sockColor) != 0) {
                    dirtyPairs += dirtySocksByColor.get(sockColor) / 2;
                }
            }
            cleanMatches += (Math.min(capacity, dirtyPairs));
            System.out.println("capacity: " + capacity);
            System.out.println("dirtyPairs" + dirtyPairs);
        }

        System.out.println("CleanMatches: " + cleanMatches);

        return cleanMatches;
    }

    public int solutionBF(int capacity, int[] clean, int[] dirty) {

        System.out.println("W: " + capacity + "; C: " + Arrays.toString(clean) + "; D: " + Arrays.toString(dirty));

        int cleanMatches = 0;
        Map<Integer, Integer> cleanSocksByColor = new HashMap<>();
        Map<Integer, Integer> dirtySocksByColor = new HashMap<>();

        // pile all socks by color
        for (int i : clean) {
            cleanSocksByColor.put(i, cleanSocksByColor.getOrDefault(i, 0) + 1);
        }
        for (int i : dirty) {
            dirtySocksByColor.put(i, dirtySocksByColor.getOrDefault(i, 0) + 1);
        }

        System.out.println("CleanPile: " + cleanSocksByColor);
        System.out.println("DirtyPile: " + dirtySocksByColor);

        //remove clean matches from clean pile
        for (int sockColor : cleanSocksByColor.keySet()) {
            cleanMatches += (cleanSocksByColor.get(sockColor) / 2);
            if (cleanSocksByColor.get(sockColor) % 2 != 0) {
                cleanSocksByColor.put(sockColor, 1);
            } else {
                cleanSocksByColor.put(sockColor, 0);
            }
        }

        System.out.println("CleanMatches: " + cleanMatches);
        System.out.println("CleanPile: " + cleanSocksByColor);

        for (int sockColor : cleanSocksByColor.keySet()) {
            if (capacity > 0) {
                if (cleanSocksByColor.get(sockColor) != 0) {
                    if (dirtySocksByColor.containsKey(sockColor)) {
                        capacity--;
                        cleanMatches++;
                        dirtySocksByColor.put(sockColor, dirtySocksByColor.get(sockColor) - 1);
                    }
                }
            }
        }

        System.out.println("CleanMatches: " + cleanMatches);
        System.out.println("DirtyPile: " + dirtySocksByColor);

        if (capacity > 1) {
            capacity = (capacity / 2);
            int dirtyPairs = 0;
            for (int sockColor : dirtySocksByColor.keySet()) {
                if (dirtySocksByColor.get(sockColor) != 0) {
                    dirtyPairs += dirtySocksByColor.get(sockColor) / 2;
                }
            }
            cleanMatches += Math.min(capacity, dirtyPairs);
            System.out.println("capacity: " + capacity);
            System.out.println("dirtyPairs" + dirtyPairs);
        }

        System.out.println("CleanMatches: " + cleanMatches);

        return cleanMatches;
    }
}
