package coderbyte;

public class QuestionMark {

    public String questionMark(String str) {

        char[] strArr = str.toCharArray();

        for (int i = 0; i < strArr.length; i++) {
            for (int j = 4 + i; j < strArr.length; j++) {
                if (Character.isDigit(strArr[i]) && Character.isDigit(strArr[j])) {
                    String subst = str.substring(i, j + 1);
                    if (validateQuestionMarks(subst)) {
                        return "true";
                    }
                }
            }
        }

        return "false";
    }

    public static Boolean validateQuestionMarks(String str) {
        int questionMarks = 0;
        char[] strArr = str.toCharArray();
        for (int i = 1; i < strArr.length; i++) {
            if (strArr[i] == '?') {
                questionMarks++;
            }
        }
        return (questionMarks == 3);
    }
}