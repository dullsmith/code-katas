package romannumerals

class ConvertFromRomanVariantNumerals : ConvertFromRomanNumerals() {

    // Step one, define a simple - even clumsy - way to map values
    override fun convertFromRomanNumerals(romanNumeral: String): Int {

        validateInput(romanNumeral)

        var holder = 0

        for (numeral in romanNumeral.toCharArray()) {
            holder += when (numeral) {
                'I' -> 1
                'V' -> 5
                'X' -> 10
                'L' -> 50
                'C' -> 100
                'D' -> 500
                'M' -> 1000
                else -> throw IllegalArgumentException("Character $numeral is not a valid roman numeral")
            }
        }
        return holder
    }
}