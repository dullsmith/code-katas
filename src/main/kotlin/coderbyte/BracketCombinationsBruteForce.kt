package coderbyte

class BracketCombinationsBruteForce {

    fun bracketCombinations(num: Int): Int {
        var total = 0
        for (i in 0 until num - 1) {
            total += (num - i)
        }
        return total
    }

}
