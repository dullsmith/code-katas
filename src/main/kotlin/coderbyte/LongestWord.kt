package coderbyte;

class LongestWord {

    fun longestWord(sen: String): String {
        return sen
            // split string by space character
            .split(" ")
            // remove punctuation
            .map {
                it.toCharArray()
                    .filter { c ->
                        c.isLetterOrDigit()
                    }
                    .joinToString(separator = "")
            }
            // return fist largest item in the list
            .maxByOrNull { it.length }!!
    }
}
