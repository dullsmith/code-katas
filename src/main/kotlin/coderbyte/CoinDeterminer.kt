package coderbyte

class CoinDeterminer {

    fun coinDeterminer(num: Int): Int {

        val coinArray = intArrayOf(1, 5, 7, 9, 11).sorted()
        val amountMem = IntArray(num + 1) { Int.MAX_VALUE }
        amountMem[0] = 0

        fun dynamicCounting(remainder: Int): Int {
            if (remainder <= 1) return 1
            if (coinArray.contains(remainder)) return 1

            var min = remainder + 1

            for (i in coinArray) {
                if (remainder - i > 0) {
                    val coin = dynamicCounting(remainder - i)
                    if (coin >= 0) {
                        min = minOf(min, coin + 1)
                    }
                }
            }
            amountMem[remainder] = min
            return amountMem[remainder]
        }

        return dynamicCounting(num)
    }

    fun coinDeterminerMin(num: Int): Int {

        val coins = intArrayOf(1, 5, 7, 9, 11).sorted()
        val amountArray = IntArray(num + 1) { num + 1 }
        amountArray[0] = 0

        println(coins)

        for (coin in coins) {
            for (i in coin until amountArray.size) {
                amountArray[i] = minOf(amountArray[i], amountArray[i - coin] + 1)
            }
        }
        println(amountArray.contentToString())

        return amountArray[num]
    }

    fun coinDeterminerOrigin(num: Int): Int {

        val coins = intArrayOf(1, 5, 7, 9, 11).sorted()
        val amountArray = IntArray(num + 1) { num + 1 }
        amountArray[0] = 0

        println(coins)

        for (coin in coins) {
            for (i in coin until amountArray.size) {
//                amountArray[i] = amountArray[i].coerceAtMost(1 + amountArray[i - coin])
                if (amountArray[i] > (amountArray[i - coin] + 1)) {
                    amountArray[i] = (amountArray[i - coin] + 1)
                }

            }
        }
        println(amountArray.contentToString())

        return amountArray[num]
    }
}