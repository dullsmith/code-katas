package coderbyte

class TreeConstructor {

    //no node can have more than 3 connections total
    fun treeConstructor(strArr: Array<String>): String {

        val listPairs = strArr.map {
            it.replace("(", "")
                .replace(")", "")
                .split(",")
        }.toList()

        val nodeMap = mutableMapOf<String, Set<String>>()

        for ((children, parent) in listPairs) {
            val childrenSet = nodeMap[parent] ?: emptySet()
            val parentSet = nodeMap[children] ?: emptySet()

            nodeMap[parent] = childrenSet + children
            nodeMap[children] = parentSet + parent
        }

        println(nodeMap)

        return (nodeMap.values.none { it.size > 3 }).toString()
    }

    fun treeConstructorNode(strArr: Array<String>): String {

        val listPairs = strArr.map {
            it.replace("(", "")
                .replace(")", "")
                .split(",")
        }.toList()

        //first is children, a children can only have one parent
        //second is parent, a parent can only have two children
        val childrenMap = mutableMapOf<String, Set<String>>()
        val parentMap = mutableMapOf<String, Set<String>>()

        for ((children, parent) in listPairs) {
            val childrenSet = childrenMap[parent] ?: emptySet()
            val parentSet = parentMap[children] ?: emptySet()

            childrenMap[parent] = childrenSet + children
            parentMap[children] = parentSet + parent
        }

        println(childrenMap)
        println(parentMap)

        return (parentMap.values.none { it.size > 1 } && childrenMap.values.none { it.size > 2 }).toString()
    }
}
