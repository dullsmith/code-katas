package coderbyte

class QuestionMarks {

    fun questionsMarks(str: String): String {

        for (i in str.indices) {
            for (j in 5 + i until str.length) {
                val substr = str.substring(i, j + 1)

                val firstValue = substr.first().takeIf { it.isDigit() }?.digitToInt()
                val lastValue = substr.last().takeIf { it.isDigit() }?.digitToInt()

                if (firstValue != null && lastValue != null && firstValue + lastValue == 10) {
                    return (substr.toCharArray()
                        .count { it == '?' } == 3).toString()
                }

            }
        }

        return "false"
    }

    fun questionsMarksBF(str: String): String {
        //string contains digits, letters and questions marks
        //check all pairs of numbers with ? in between that add to 10

        for (i in str.indices) {
            for (j in 5 + i until str.length) {
                val substr = str.substring(i, j + 1)
                if (substr.first().isDigit() && substr.last().isDigit()) {
                    if (substr.first().digitToInt() + substr.last().digitToInt() == 10) {
                        println(substr)
                        return (substr.toCharArray()
                            .count { it == '?' } == 3).toString()
                    }
                }
            }
        }

        return "false"
    }

}
