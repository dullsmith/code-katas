package coderbyte

class FindIntersection {

    fun findIntersection(strArr: Array<String>): String {
        val firstList = strArr.first()
            .filterNot { it == ' ' }
            .split(",")
        val secondList = strArr.last()
            .filterNot { it == ' ' }
            .split(",")

        println(firstList)
        println(secondList)

        val resultList = mutableListOf<String>()

        firstList.forEach {
            if (secondList.contains(it)) {
                resultList.add(it)
            }
        }

        println(resultList)
        println(resultList.toString())
        return resultList.joinToString(separator = ",")
    }
}
