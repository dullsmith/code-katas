package coderbyte

class CodelandUsernameValidation {

    fun codelandUsernameValidation(str: String): String {

        if (str.length !in 4..25) {
            return "false"
        }

        if (!str.first().isLetter() || str.last() == '_') {
            return "false"
        }

        if (str
                .toCharArray()
                .filter { !it.isLetterOrDigit() }
                .any { it != '_' }
        ) {
            return "false"
        }

        return "true"
    }

}