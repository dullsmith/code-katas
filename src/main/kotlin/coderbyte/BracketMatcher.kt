package coderbyte

import java.util.Stack


class BracketMatcher {

    fun bracketMatcherBruteForce(str: String): String {

        val openBrackets = str.toCharArray().count {
            it == '('
        }
        val closeBrackets = str.toCharArray().count {
            it == ')'
        }

        return if (openBrackets == closeBrackets) "1" else "0"
    }

    fun bracketMatcherFunctional(str: String): String {

        val notMatchedBrackets = str.toCharArray()
            .count {
                it == '('
            }
            .minus(str
                .toCharArray()
                .count {
                    it == ')'
                }
            )
        print(notMatchedBrackets)
        return if (notMatchedBrackets == 0) "1" else "0"
    }

    fun bracketMatcher(str: String): String {
        val openBracketStack = Stack<Char>()
        val closeBracketStack = Stack<Char>()

        str.toCharArray()
            .map {
                if (it == '(') {
                    openBracketStack.push(it)
                }
            }
        str.toCharArray()
            .map {
                if (it == ')') {
                    closeBracketStack.push(it)
                }
            }

        return if (openBracketStack.size == closeBracketStack.size) "1" else "0"
    }
}