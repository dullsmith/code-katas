package coderbyte

class MinWindowSubstring {

    fun minWindowSubstring(strArr: Array<String>): String {
        val inputString = strArr.first()
        val matcher = strArr.last()
        println(matcher)

        //pointer starting in the minimum value possible
        for (i in matcher.length..inputString.length) {
            //second pointer starting in 0 and going to the largest value possible
            for (j in 0..(inputString.length - i)) {
                val slice = inputString.substring(j, j + i)
                if (containsSubString(slice, matcher)) return slice
            }
        }
        return ""
    }

    private fun containsSubString(slice: String, matcher: String): Boolean {
        val matchCharCountMap = matcher.groupingBy { it }.eachCount().toMap()
        val sliceCharCountMap = slice.groupingBy { it }.eachCount().toMap()

        if (!sliceCharCountMap.keys.containsAll(matchCharCountMap.keys)) {
            return false
        } else {
            matchCharCountMap.forEach {
                if (sliceCharCountMap[it.key]!! < it.value) {
                    return false
                }
            }
        }

        println(slice)
        return true
    }
}
