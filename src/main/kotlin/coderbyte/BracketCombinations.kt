package coderbyte

class BracketCombinations {

    fun bracketCombinations(num: Int): Int {
        return if (num in 0..2) {
            num
        } else {
            (factorial(num * 2) / (factorial(num + 1) * factorial(num))).toInt()
        }
    }

    private fun factorial(num: Int): Long {
        var fact: Long = 1
        for (i in 1..num) {
            fact *= i
        }
        return fact
    }
}
