package codility

class LongestPassword {

    fun solution(str: String): Int {

        val wordList = str.split(' ')
            .filter {
                it.filter { c -> !c.isLetterOrDigit() }.isBlank()
            }
            .filterNot {
                it.count { c -> c.isLetter() }.rem(2) != 0
            }
            .filterNot {
                it.count { c -> c.isDigit() }.rem(2) == 0
            }

        if (wordList.isEmpty()) {
            return -1
        }

        return wordList.maxOf { it.length }
    }
}
