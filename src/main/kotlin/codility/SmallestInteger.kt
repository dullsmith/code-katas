package codility

class SmallestInteger {
    fun solution(intArray: IntArray): Int {
        val sortedPositiveInput = intArray.filter { it > 0 }.distinct().sorted()
        val amountArray = IntArray(sortedPositiveInput.size + 1) { 0 }
        for (i in amountArray.indices) {
            if (!sortedPositiveInput.contains(i)) {
                amountArray[i] = i
            }
        }

        val result = amountArray.filter { it > 0 }
        println(result)

        return if (result.isNotEmpty()) {
            result.first()
        } else {
            sortedPositiveInput.size + 1
        }
    }

    fun solution2(intArray: IntArray): Int {
        val sortedPositiveInput = intArray.filter { it > 0 }.distinct().sorted()
        val amountArray = IntArray(sortedPositiveInput.size + 1) { Int.MAX_VALUE }
        for (i in 1 until amountArray.size) {
            if (!sortedPositiveInput.contains(i)) {
                amountArray[i] = i
            }
        }
        println(amountArray.contentToString())

        return minOf(amountArray.min(), sortedPositiveInput.size + 1)
    }

}