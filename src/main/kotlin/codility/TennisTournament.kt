package codility

class TennisTournament {
    fun solution(players: Int, courts: Int): Int {

        val pairs = players.div(2)

        return if (pairs < courts) {
            pairs
        } else courts

    }
}
