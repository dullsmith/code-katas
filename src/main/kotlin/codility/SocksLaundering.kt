package codility

class SocksLaundering {
    fun solution(washingCapacity: Int, clean: IntArray, dirty: IntArray): Int {

        println("W: $washingCapacity, C: ${clean.contentToString()}, D: ${dirty.contentToString()}")

        var cleanMatchedPairs = 0
        val cleanUnmatchedPairs = mutableListOf<Int>()
        var washingSlots = washingCapacity
        val dirtySocks = dirty.toMutableList()

        clean.forEach {
            if (cleanUnmatchedPairs.contains(it)) {
                cleanMatchedPairs += 1
                cleanUnmatchedPairs.remove(it)
            } else {
                cleanUnmatchedPairs.add(it)
            }
        }

        println("CU: $cleanUnmatchedPairs")
        println("IDS: $dirtySocks")

        cleanUnmatchedPairs.forEach {
            if (dirtySocks.contains(it) && washingSlots > 0) {
                washingSlots--
                dirtySocks.remove(it)
                cleanMatchedPairs++
            }
        }

        println("RDS: $dirtySocks")
        println("CP: $cleanMatchedPairs")

        if (washingSlots > 1) {
            washingSlots = washingSlots.div(2)

            val remainingPairs = dirtySocks.groupBy { it }
                .map { it.value.count().div(2) }
                .sum()

            cleanMatchedPairs += minOf(washingSlots, remainingPairs)

            println("R: $remainingPairs")

        }

        return cleanMatchedPairs
    }

    fun solutionBF(washingCapacity: Int, clean: IntArray, dirty: IntArray): Int {

        println("W: $washingCapacity, C: ${clean.contentToString()}, D: ${dirty.contentToString()}")

        var cleanMatchedPairs = 0
        val cleanUnmatchedPairs = mutableListOf<Int>()

        clean.forEach {
            if (cleanUnmatchedPairs.contains(it)) {
                cleanMatchedPairs += 1
                cleanUnmatchedPairs.remove(it)
            } else {
                cleanUnmatchedPairs.add(it)
            }
        }

        println("CU: $cleanUnmatchedPairs")

        var washingSlots = washingCapacity
        val dirtySocks = dirty.toMutableList()
        println("IDS: $dirtySocks")

        cleanUnmatchedPairs.forEach {
            if (dirtySocks.contains(it) && washingSlots > 0) {
                washingSlots--
                dirtySocks.remove(it)
                cleanMatchedPairs++
            }
        }

        println("RDS: $dirtySocks")
        println("CP: $cleanMatchedPairs")

        if (washingSlots > 1) {
            washingSlots = washingSlots.div(2)

            val remainingPairs = dirtySocks.groupBy { it }
                .filter { it.value.count() > 1 }
                .map { it.value.count().div(2) }
                .sum()

            if (washingSlots < remainingPairs) {
                cleanMatchedPairs += washingSlots
            } else {
                cleanMatchedPairs += remainingPairs
            }

            println("R: $remainingPairs")

        }

        return cleanMatchedPairs
    }
}
