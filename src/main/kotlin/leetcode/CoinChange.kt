package leetcode

// Source: https://leetcode.com/problems/coin-change/
class CoinChange {

    fun fewestCoinsToAmount(coins: IntArray, amount: Int): Int {
        val dAmounts = IntArray(amount + 1) { amount + 1 }
        dAmounts[0] = 0

        for (coin in coins.sorted()) {
            for (i in coin until dAmounts.size) {
                dAmounts[i] = dAmounts[i].coerceAtMost(1 + dAmounts[i - coin])
            }
        }
        return if (dAmounts[amount] > amount) -1 else dAmounts[amount]
    }

    fun fewestCoinsToAmountFunctional(coins: IntArray, amount: Int): Int {
        val dAmounts = IntArray(amount + 1)
        (1..amount).forEach { stepAmount ->
            dAmounts[stepAmount] = coins.asSequence()
                .filter { coin ->
                    stepAmount - coin >= 0 && dAmounts[stepAmount - coin] >= 0
                }
                .map {
                    1 + dAmounts[stepAmount - it]
                }
                .minOrNull() ?: -1
        }
        return dAmounts[amount]
    }
}