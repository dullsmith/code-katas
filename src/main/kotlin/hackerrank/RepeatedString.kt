package hackerrank
//Source: https://www.hackerrank.com/challenges/repeated-string/
class RepeatedString {
    fun repeatedString(s: String, n: Long): Long {
        val remainder = s.substring(0, n.rem(s.length).toInt()).count { it == 'a' }
        return s
            .count { it == 'a' }
            .times(n.div(s.length))
            .plus(remainder)
    }
}
