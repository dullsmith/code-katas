package hackerrank//Source: https://www.hackerrank.com/challenges/sock-merchant/

class SalesByMatch() {
    fun sockMerchant(numberOfSocks: Int, socks: Array<Int>): Int {
        assert(socks.size == numberOfSocks)
        return socks.groupBy { it }.map { it.value.count().div(2) }.sum()
    }
}
