package hackerrank

//Source: https://www.hackerrank.com/challenges/counting-valleys/problem
class CountingValleys {

    //This implementation highly depends upon the rule that every hike always stars and ends at sea level (0)

    fun countingValleysBruteForce(steps: Int, path: String): Int {
        assert(path.length == steps)

        var seaLevel = 0
        var valleys = 0

        for (char in path) {
            if (char == 'D') {
                seaLevel -= 1
                if (seaLevel == -1) {
                    valleys += 1
                }
            } else {
                seaLevel += 1
            }
        }
        assert(seaLevel == 0)
        return valleys
    }

    fun countingValleys(steps: Int, path: String): Int {

        assert(path.length == steps)

        var seaLevel = 0
        var valleys = 0

        path.forEach {
            if (it == 'D') {
                if (--seaLevel == -1) valleys++
            } else {
                seaLevel++
            }
        }

        assert(seaLevel == 0)

        return valleys
    }
}
