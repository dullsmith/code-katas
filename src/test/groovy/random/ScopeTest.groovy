package random

import spock.lang.Specification
import spock.lang.Unroll

class ScopeTest extends Specification {

    static def getChangeScope(commitLogs) {
        if (commitLogs.size() == 0) {
            return 'BUILD'
        }
        return ['MAJOR', 'MINOR', 'REVISION']
                .find { commitMsg -> commitLogs.any { it.startsWith(commitMsg) } }
                ?: 'REVISION'
    }

    @Unroll
    def 'Should return #output given #input'() {

        when:
        def result = getChangeScope(input)

        then:
        output == result

        where:
        output     || input
        "BUILD"    || []
        "REVISION" || ["some test", "not relevant", "not starting with MAJOR"]
        "MAJOR"    || ["MINOR | FIN", "MAJOR | FIN"]
        "MINOR"    || ["MINOR | FIN", "SOMETHING before MAJOR"]
        "MINOR"    || ["MINOR  ", "REVISION "]
        "MINOR"    || ["REVISION  ", "REVISION ", "REVISION ", "MINOR"]
    }

}
