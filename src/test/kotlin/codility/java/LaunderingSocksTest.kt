package codility.java

import codility.LaunderingSocks
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory

class LaunderingSocksTest {

    private val sut = LaunderingSocks()

    @TestFactory
    fun inputList() = listOf(
        Triple(2, intArrayOf(1, 2, 1, 1), intArrayOf(1, 4, 3, 2, 4)) to 3,
        Triple(1, intArrayOf(1, 2), intArrayOf(1, 4, 3, 2, 4)) to 1,
        Triple(50, intArrayOf(1, 2, 3, 4, 5), intArrayOf(1, 4, 3)) to 3,
        Triple(0, intArrayOf(1, 1, 3, 4, 5), intArrayOf(1, 4, 3)) to 1,
        Triple(0, intArrayOf(1, 2, 3, 4, 5), intArrayOf(1, 4, 3)) to 0,
        Triple(50, intArrayOf(1, 1, 1, 1, 1), intArrayOf(1, 2, 2)) to 4,
        Triple(4, intArrayOf(1), intArrayOf(2, 2, 2, 2)) to 2,
        Triple(4, intArrayOf(1), intArrayOf(2, 2, 2, 2, 5, 4, 7, 3)) to 2,
        Triple(4, intArrayOf(1), intArrayOf(2, 2, 2, 2, 2, 5, 4, 7, 3, 3)) to 2,
        Triple(1, intArrayOf(1, 1), intArrayOf(2, 2, 2, 2)) to 1,
    )
        .map {
            DynamicTest.dynamicTest("should return: ${it.second}") {
                Assertions.assertEquals(it.second, sut.solution(it.first.first, it.first.second, it.first.third))
            }
        }

}
