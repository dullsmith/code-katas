package codility

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory

class LongestPasswordTest {

    private val sut = LongestPassword()


    @TestFactory
    fun inputList() = listOf(
        "test 5 a0A pass007 ?xy1" to 7,
        "a" to -1,
        "ana" to -1,
    )
        .map {
            DynamicTest.dynamicTest("should return: ${it.second}") {
                Assertions.assertEquals(it.second, sut.solution(it.first))
            }
        }
}
