package codility

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory

class TennisTournamentTest {

    private val sut = TennisTournament()

    @TestFactory
    fun inputList() = listOf(
        listOf(5, 3) to 2,
        listOf(10, 3) to 3,
        listOf(1, 2) to 0,
        listOf(30000, 2) to 2,
        listOf(30000, 10000) to 10000,
    )
        .map {
            DynamicTest.dynamicTest("should return: ${it.second}") {
                Assertions.assertEquals(it.second, sut.solution(it.first.first(), it.first.last()))
            }
        }
}
