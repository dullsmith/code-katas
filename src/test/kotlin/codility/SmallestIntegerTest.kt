package codility

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory

class SmallestIntegerTest {

    private val sut = SmallestInteger()

    @TestFactory
    fun inputList() = listOf(
        intArrayOf(1, 3, 6, 4, 1, 2) to 5,
        intArrayOf(1, 2, 3) to 4,
        intArrayOf(-1, -2) to 1,
        intArrayOf(3, 7, 90) to 1,
    )
        .map {
            DynamicTest.dynamicTest("for ${it.first.contentToString()} should return: ${it.second}") {
                Assertions.assertEquals(it.second, sut.solution(it.first))
            }
        }
}