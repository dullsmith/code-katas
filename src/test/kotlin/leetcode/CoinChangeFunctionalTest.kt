package leetcode

import leetcode.CoinChange
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class CoinChangeFunctionalTest {

    private val coinChange = CoinChange()

    @Test
    fun `Should succeed for int array`() {
        //given
        val inputCoins = intArrayOf(1, 2, 5)
        val amount = 11
        //when
        val result = coinChange.fewestCoinsToAmountFunctional(inputCoins, amount)
        //then
        Assertions.assertEquals(3, result)
    }

    @Test
    fun `Should return -1 when impossible amount for given coins`() {
        //given
        val inputCoins = intArrayOf(2)
        val amount = 3
        //when
        val result = coinChange.fewestCoinsToAmountFunctional(inputCoins, amount)
        //then
        Assertions.assertEquals(-1, result)
    }

    @Test
    fun `Should succeed when amount is zero`() {
        //given
        val inputCoins = intArrayOf(1)
        val amount = 0
        //when
        val result = coinChange.fewestCoinsToAmountFunctional(inputCoins, amount)
        //then
        Assertions.assertEquals(0, result)
    }

    @Test
    fun `Should succeed when given coins have no 1 cent`() {
        //given
        val inputCoins = intArrayOf(3, 4, 5)
        val amount = 7
        //when
        val result = coinChange.fewestCoinsToAmountFunctional(inputCoins, amount)
        //then
        Assertions.assertEquals(2, result)
    }

    @Test
    fun `Should succeed for unordered coin array`() {
        //given
        val inputCoins = intArrayOf(5, 3, 4, 2)
        val amount = 7
        //when
        val result = coinChange.fewestCoinsToAmountFunctional(inputCoins, amount)
        //then
        Assertions.assertEquals(2, result)
    }
}