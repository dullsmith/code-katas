package leetcode

import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class ValidAnagramFunctionalTest {

    private val anagram = ValidAnagram()

    @Test
    fun `Should return true if it is an anagram`() {
        //when
        val result = anagram.isAnagramFunctional("anagram", "nagaram")
        //then
        assertTrue(result)
    }

    @Test
    fun `Should false if it is not an anagram`() {
        //when
        val result = anagram.isAnagramFunctional("rat", "car")
        //then
        assertFalse(result)
    }

}