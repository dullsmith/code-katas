package hackerrank

import hackerrank.SalesByMatch
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class SalesByMatchTest {

    private val salesByMatch = SalesByMatch()

    @Test
    fun `given a list to sort return must be 3`() {
        //given:
        val socks = arrayOf(10, 20, 20, 10, 10, 30, 50, 10, 20)
        //when:
        val st = salesByMatch.sockMerchant(9, socks)
        //then:
        Assertions.assertEquals(st, 3)
    }

    @Test
    fun `given a list to sort return must be 4`() {
        //given:
        val socks = arrayOf(1, 1, 3, 1, 2, 1, 3, 3, 3, 3)
        //when:
        val st = salesByMatch.sockMerchant(10, socks)
        //then:
        Assertions.assertEquals(st, 4)
    }

    @Test
    fun `given a wrong number of socks fail`() {
        assertThrows<AssertionError> {
            salesByMatch.sockMerchant(1, arrayOf(1, 1, 3, 1, 2, 1, 3, 3, 3, 3))
        }
    }
}


