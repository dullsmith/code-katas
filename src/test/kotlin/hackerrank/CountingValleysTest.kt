package hackerrank

import hackerrank.CountingValleys
import org.junit.Assert
import org.junit.Test

class CountingValleysTest {

    private val valleys = CountingValleys()

    @Test
    fun `count 1 valleys`() {
        //when:
        val st = valleys.countingValleys(8, "UDDDUDUU")
        //then:
        Assert.assertEquals(st, 1)
    }

    @Test
    fun `count 6 valleys`() {
        //when:
        val st = valleys.countingValleys(18, "DDUUUDDUDUDUDUDDUU")
        //then:
        Assert.assertEquals(st, 6)
    }

    @Test
    fun `count 2 valleys`() {
        //when:
        val st = valleys.countingValleys(12, "DDUUDDUDUUUD")
        //then:
        Assert.assertEquals(st, 2)
    }

    @Test
    fun `count 8 valleys`() {
        //when:
        val st = valleys.countingValleysBruteForce(22, "DDUUUDDUDUDUDUDDUUDUDU")
        //then:
        Assert.assertEquals(st, 8)
    }
}

