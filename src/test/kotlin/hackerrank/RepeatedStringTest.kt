package hackerrank

import hackerrank.RepeatedString
import org.junit.Assert
import org.junit.Test

internal class RepeatedStringTest {

    private val repeatedString = RepeatedString()

    @Test
    fun `successfully find all repetitions of character 'a'`() {
        //given:
        val input = "aba"
        //when:
        val st = repeatedString.repeatedString(input, 10)
        //then:
        Assert.assertEquals(st, 7)
    }

    @Test
    fun `successfully find all repetitions of 'a'`() {
        //given:
        val input = "a"
        //when:
        val st = repeatedString.repeatedString(input, 10000000000)
        //then:
        Assert.assertEquals(st, 10000000000)
    }

    @Test
    fun `successful when 'a' is not present`() {
        //given:
        val input = "b"
        //when:
        val st = repeatedString.repeatedString(input, 987)
        //then:
        Assert.assertEquals(st, 0)
    }

    @Test
    fun `successful when 'a' is present`() {
        //given:
        val input = "jdiacikk"
        //when:
        val st = repeatedString.repeatedString(input, 899491)
        //then:
        Assert.assertEquals(st, 112436)
    }

    @Test
    fun `successful when 'a' is present 2`() {
        //given:
        val input = "bab"
        //when:
        val st = repeatedString.repeatedString(input, 725261545450)
        //then:
        Assert.assertEquals(st, 241753848483)
    }
}
