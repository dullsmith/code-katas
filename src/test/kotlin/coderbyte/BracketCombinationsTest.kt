package coderbyte

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory

class BracketCombinationsTest {

    private val sut = BracketCombinations()

    @TestFactory
    fun inputList() = listOf(
        3 to 5,
        2 to 2,
    )
        .map {
            DynamicTest.dynamicTest("should return: ${it.second}") {
                Assertions.assertEquals(it.second, sut.bracketCombinations(it.first))
            }
        }

    private val sutBF = BracketCombinationsBruteForce()

    @TestFactory
    fun inputListBF() = listOf(
        3 to 5,
        2 to 2,
    )
        .map {
            DynamicTest.dynamicTest("should return: ${it.second}") {
                Assertions.assertEquals(it.second, sutBF.bracketCombinations(it.first))
            }
        }
}
