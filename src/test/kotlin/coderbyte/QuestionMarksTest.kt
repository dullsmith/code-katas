package coderbyte

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory

class QuestionMarksTest {

    private val sut = QuestionMarks()

    @TestFactory
    fun inputList() = listOf(
        "aa6?9" to "false",
        "acc?7??sss?3rr1??????5" to "true",
        "arrb6???4xxbl5???eee5" to "true",
        "9???1???9??1???9" to "false",
        "5??aaaaaaaaaaaaaaaaaaa?5?5" to "false",
        "4?5?4?aamm9" to "false"
    ).map {
        DynamicTest.dynamicTest("should return: ${it.second}") {
            assertEquals(it.second, sut.questionsMarks(it.first))
        }
    }
}
