package coderbyte

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory

class CoinDeterminerTest {
    private val sut = CoinDeterminer()

    @TestFactory
    fun testInput() = listOf(
        6 to 2,
        16 to 2,
        25 to 3,
        3 to 3,
        1 to 1,
        11 to 1,
        8 to 2
    )
        .map {
            DynamicTest.dynamicTest("should return: ${it.second}") {
                Assertions.assertEquals(it.second, sut.coinDeterminer(it.first))
            }
        }

    @TestFactory
    fun testInputOrigin() = listOf(
        6 to 2,
        16 to 2,
        25 to 3,
        3 to 3,
        1 to 1,
        11 to 1,
        8 to 2
    )
        .map {
            DynamicTest.dynamicTest("should return: ${it.second}") {
                Assertions.assertEquals(it.second, sut.coinDeterminerOrigin(it.first))
            }
        }

    @TestFactory
    fun testInputMin() = listOf(
        6 to 2,
        16 to 2,
        25 to 3,
        3 to 3,
        1 to 1,
        11 to 1,
        8 to 2
    )
        .map {
            DynamicTest.dynamicTest("should return: ${it.second}") {
                Assertions.assertEquals(it.second, sut.coinDeterminerMin(it.first))
            }
        }
}