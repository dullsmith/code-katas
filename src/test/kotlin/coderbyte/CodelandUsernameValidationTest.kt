package coderbyte

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory


class CodelandUsernameValidationTest {

    private val sut = CodelandUsernameValidation()

    @TestFactory
    fun testInput() = listOf(
        "aa_" to "false",
        "u__hello_world123" to "true",
        "usernamehello123" to "true",
        "a______b_________555555555555aaaa" to "false",
        "123abc444" to "false",
        "oooooooooooooooooo________a" to "false"
    )
        .map {
            DynamicTest.dynamicTest("should return: ${it.second}") {
                Assertions.assertEquals(it.second, sut.codelandUsernameValidation(it.first))
            }
        }
}
