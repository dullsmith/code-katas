package coderbyte


import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory

class LongestWordTest {

    private val sut = LongestWord()

    @TestFactory
    fun testInput() = listOf(
        "fun&!! time" to "time",
        "I love dogs" to "love",
        "Hello world123 567" to "world123",
        "this is some sort of sentence" to "sentence",
        "coderbyte" to "coderbyte",
        "letter after letter!!" to "letter",
        "a confusing /:sentence:/[ this is not!!!!!!!~" to "confusing",
        "123456789 98765432" to "123456789"

    )
        .map {
            DynamicTest.dynamicTest("should return: ${it.second}") {
                assertEquals(it.second, sut.longestWord(it.first))
            }
        }

}
