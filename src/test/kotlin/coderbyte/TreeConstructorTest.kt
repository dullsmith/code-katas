package coderbyte

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory

class TreeConstructorTest {

    private val sut = TreeConstructor()

    @TestFactory
    fun testInput() = listOf(
        arrayOf("(1,2)", "(2,4)", "(5,7)", "(7,2)", "(9,5)") to "true",
        arrayOf("(1,2)", "(3,2)", "(2,12)", "(5,2)") to "false",
        arrayOf("(1,2)", "(2,4)", "(7,2)") to "true",
    )
        .map {
            DynamicTest.dynamicTest("should return: ${it.second}") {
                Assertions.assertEquals(it.second, sut.treeConstructor(it.first))
            }
        }
}
