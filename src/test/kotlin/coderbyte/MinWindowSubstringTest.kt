package coderbyte

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory


class MinWindowSubstringTest {


    private val sut = MinWindowSubstring()

    @TestFactory
    fun testInput() = listOf(
        arrayOf("ahffaksfajeeubsne", "jefaa") to "aksfaje",
        arrayOf("aaffhkksemckelloe", "fhea") to "affhkkse",
        arrayOf("aabdccdbcacd", "aad") to "aabd",
        arrayOf("aaabaaddae", "aed") to "dae",
    )
        .map {
            DynamicTest.dynamicTest("should return: ${it.second}") {
                Assertions.assertEquals(it.second, sut.minWindowSubstring(it.first))
            }
        }

}
