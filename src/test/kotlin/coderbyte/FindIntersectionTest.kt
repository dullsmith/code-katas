package coderbyte

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory

class FindIntersectionTest {

    private val sut = FindIntersection()

    @TestFactory
    fun inputList() = listOf(
        arrayOf("1, 3, 4, 7, 13", "1, 2, 4, 13, 15") to "1,4,13",
        arrayOf("1, 3, 9, 10, 17, 18", "1, 4, 9, 10") to "1,9,10",
        arrayOf("2, 5, 7, 10, 11, 12", "2, 7, 8, 9, 10, 11, 15") to "2,7,10,11",
        arrayOf("21, 22, 23, 25, 27, 28", "21, 24, 25, 29") to "21,25"
    ).map {
        DynamicTest.dynamicTest("should return: ${it.second}") {
            Assertions.assertEquals(it.second, sut.findIntersection(it.first))
        }
    }
}
