package coderbyte

import coderbyte.BracketMatcher
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class BracketMatcherTest {

    private val bracketMatcher = BracketMatcher()

    @Test
    fun `validate brackets match correctly - brute`() {
        //given
        val input = "(coder)(byte)"
        //when
        val result = bracketMatcher.bracketMatcherBruteForce(input)
        //then
        assertEquals("1", result)
    }

    @Test
    fun `validate brackets don't match correctly - brute`() {
        //given
        val input = "(coder)(byte))"
        //when
        val result = bracketMatcher.bracketMatcherBruteForce(input)
        //then
        assertEquals("0", result)
    }
    @Test
    fun `validate brackets match correctly`() {
        //given
        val input = "(coder)(byte)"
        //when
        val result = bracketMatcher.bracketMatcher(input)
        //then
        assertEquals("1", result)
    }

    @Test
    fun `validate brackets don't match correctly`() {
        //given
        val input = "(coder)(byte))"
        //when
        val result = bracketMatcher.bracketMatcher(input)
        //then
        assertEquals("0", result)
    }
}